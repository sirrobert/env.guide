module.exports = {
  "root": true,
  "plugins": [
    "react",
    "jest"
  ],
  "settings": {
    "react": {
      "version":"17"
    }
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 6
  },
  "overrides": [
    {
      "files": ["*.js"],
      "rules": {
      }
    }
  ],
  "rules": {
    "react/react-in-jsx-scope": "off",
    "react/no-unescaped-entities": [ "error", {
      forbid: [
        {char : ">", alternatives : ['&gt;'  ]},
        {char : "<", alternatives : ['&lt;'  ]},
        {char : "}", alternatives : ['&#125;']},
      ]
    }],
  },
  "env": {
    "node": true,
    "browser": true,
    "amd": true,
    "jest":true
  },
  "extends": [
    "eslint:recommended",
    "plugin:cypress/recommended",
    "plugin:react/recommended"
  ]
};
