import { MemoryRouter, Route } from 'react-router-dom';

/**
 * decorator:router
 *
 * Lets the story author render components that require routing.  This uses
 * a memory router with just one route ('/').  This has no effect on
 * components that do not require routers.
 *
 * This is provided universally.
 *
 * @param {} Story
 * @param {} options
 */
const decorator = (Story, options) => (
  <MemoryRouter initialEntries={['/']}>
    <Route path="/">
      <Story/>
    </Route>
  </MemoryRouter>
);

export default decorator;
