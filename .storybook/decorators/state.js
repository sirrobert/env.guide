import { Provider }    from 'react-redux';
import { createStore } from '../../src/redux/store';

/**
 * decorator:state
 *
 * Lets the story author provide app state through the
 * `parameters={{state:...}}` property of the story object.
 *
 * This is provided universally.  When no state is provided, the
 * application's `defaultInitialState` (defined in `src/redux/store`) is
 * used.
 *
 * @param {} Story
 * @param {} options
 * @returns {React.node} A rendered react component wrapped in state.
 *
 * @example In your storybook story:
 *      <Story parameters={{state: myFixtureData}}/>
 *        { your story content }
 *      </Story>
 *  
 */
const decorator = (Story, options) => {
  let {state} = options?.parameters;
  let store = createStore(state);

  return (
    <Provider store={store}>
      <Story/>
    </Provider>
  );
};

export default decorator;
