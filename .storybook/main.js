const path = require('path');

module.exports = {
  core: {
    builder: "webpack5",
  },
  stories: [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)",
    "../src/**/stories.mdx",
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-controls",
    "@storybook/addon-knobs",
  ],
  webpackFinal: async (config) => {
    config.module.rules.push({
      // this is for both less and scss
      test: /.*\.(?:sc|c)ss$/,
      use: [
        "style-loader",
        {
          loader: "css-loader",
          options: {
            modules: true,
          },
        },
      ],
    });

    config.resolve.alias = {
      'theme': path.resolve(__dirname, '..', 'src', 'theme'),
    };

    return config;
  },
};
