import resolveConfig  from 'tailwindcss/resolveConfig';
import tailwindConfig from '../tailwind.config';
import decorators     from './decorators';

// Note that this happens *inside* the story's iframe, so it's isolated.
window.style = resolveConfig(tailwindConfig);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },

  // Various global decorators.
  decorators,

  viewport: {
    // TODO:  We should read these breakpoint styles (width/height) from the
    // breakpoints file, rather than using them as magic values here.
    viewports: {
      'Breakpoint: xs': {
        name: 'Breakpoint: xs',
				styles: { width: '320px', height: '640px' }
			},
			'Breakpoint: sm': {
				name: 'Breakpoint: sm',
				styles: { width: '600px', height: '920px' }
			},
			'Breakpoint: md': {
				name: 'Breakpoint: md',
				styles: { width: '768px', height: '520px' }
			},
			'Breakpoint: lg': {
				name: 'Breakpoint: lg',
				styles: { width: '1000px', height: '600px' }
			},
			'Breakpoint: xl': {
				name: 'Breakpoint: xl',
				styles: { width: '1140px', height: '920px' }
			},

			"Chromebook Pixel": {
				"name": "Chromebook Pixel",
				"styles": {
					"height": "850px",
					"width": "1280px"
				}
			},
			"Google Pixel": {
				"name": "Google Pixel",
				"styles": {
					"height": "731px",
					"width": "411px"
				}
			},
			"Google Pixel 2": {
				"name": "Google Pixel 2",
				"styles": {
					"height": "731px",
					"width": "411px"
				}
			},
			"Google Pixel 2 XL": {
				"name": "Google Pixel 2 XL",
				"styles": {
					"height": "823px",
					"width": "411px"
				}
			},
			"Google Pixel XL": {
				"name": "Google Pixel XL",
				"styles": {
					"height": "731px",
					"width": "411px"
				}
			},
			"LG G3": {
				"name": "LG G3",
				"styles": {
					"height": "640px",
					"width": "360px"
				}
			},
			"LG G4": {
				"name": "LG G4",
				"styles": {
					"height": "640px",
					"width": "360px"
				}
			},
			"LG G5": {
				"name": "LG G5",
				"styles": {
					"height": "640px",
					"width": "360px"
				}
			},
			"Nexus 5X": {
				"name": "Nexus 5X",
				"styles": {
					"height": "731px",
					"width": "411px"
				}
			},
			"Nexus 6P": {
				"name": "Nexus 6P",
				"styles": {
					"height": "731px",
					"width": "411px"
				}
			},
			"Nexus 7 (2013)": {
				"name": "Nexus 7 (2013)",
				"styles": {
					"height": "960px",
					"width": "600px"
				}
			},
			"Nexus 9": {
				"name": "Nexus 9",
				"styles": {
					"height": "1024px",
					"width": "768px"
				}
			},
			"One Plus 3": {
				"name": "One Plus 3",
				"styles": {
					"height": "853px",
					"width": "480px"
				}
			},
			"Samsung Galaxy Note 5": {
				"name": "Samsung Galaxy Note 5",
				"styles": {
					"height": "853px",
					"width": "480px"
				}
			},
			"Samsung Galaxy S7": {
				"name": "Samsung Galaxy S7",
				"styles": {
					"height": "640px",
					"width": "360px"
				}
			},
			"Samsung Galaxy S7 Edge": {
				"name": "Samsung Galaxy S7 Edge",
				"styles": {
					"height": "640px",
					"width": "360px"
				}
			},
			"Samsung Galaxy S8": {
				"name": "Samsung Galaxy S8",
				"styles": {
					"height": "740px",
					"width": "360px"
				}
			},
			"Samsung Galaxy S8+": {
				"name": "Samsung Galaxy S8+",
				"styles": {
					"height": "740px",
					"width": "360px"
				}
			},
			"Samsung Galaxy S9": {
				"name": "Samsung Galaxy S9",
				"styles": {
					"height": "740px",
					"width": "360px"
				}
			},
			"Samsung Galaxy S9+": {
				"name": "Samsung Galaxy S9+",
				"styles": {
					"height": "740px",
					"width": "360px"
				}
			},
			"Samsung Galaxy Tab 10": {
				"name": "Samsung Galaxy Tab 10",
				"styles": {
					"height": "1280px",
					"width": "800px"
				}
			},
			"iPad Air 1 & 2": {
				"name": "iPad Air 1 & 2",
				"styles": {
					"height": "1024px",
					"width": "768px"
				}
			},
			"iPad Mini": {
				"name": "iPad Mini",
				"styles": {
					"height": "1024px",
					"width": "768px"
				}
			},
			"iPad Mini 2 & 3": {
				"name": "iPad Mini 2 & 3",
				"styles": {
					"height": "1024px",
					"width": "768px"
				}
			},
			"iPad Pro": {
				"name": "iPad Pro",
				"styles": {
					"height": "1366px",
					"width": "1024px"
				}
			},
			"iPad Third & Fourth Generation": {
				"name": "iPad Third & Fourth Generation",
				"styles": {
					"height": "1024px",
					"width": "768px"
				}
			},
			"iPhone 11": {
				"name": "iPhone 11",
				"styles": {
					"height": "896px",
					"width": "414px"
				}
			},
			"iPhone 11 Pro": {
				"name": "iPhone 11 Pro",
				"styles": {
					"height": "812px",
					"width": "375px"
				}
			},
			"iPhone 11 Pro Max": {
				"name": "iPhone 11 Pro Max",
				"styles": {
					"height": "896px",
					"width": "414px"
				}
			},
			"iPhone 11 X": {
				"name": "iPhone 11 X",
				"styles": {
					"height": "812px",
					"width": "375px"
				}
			},
			"iPhone 11 Xr": {
				"name": "iPhone 11 Xr",
				"styles": {
					"height": "896px",
					"width": "414px"
				}
			},
			"iPhone 11 Xs": {
				"name": "iPhone 11 Xs",
				"styles": {
					"height": "812px",
					"width": "375px"
				}
			},
			"iPhone 11 Xs Max": {
				"name": "iPhone 11 Xs Max",
				"styles": {
					"height": "896px",
					"width": "414px"
				}
			},
			"iPhone 12": {
				"name": "iPhone 12",
				"styles": {
					"height": "844px",
					"width": "390px"
				}
			},
			"iPhone 12 Mini": {
				"name": "iPhone 12 Mini",
				"styles": {
					"height": "780px",
					"width": "360px"
				}
			},
			"iPhone 12 Pro": {
				"name": "iPhone 12 Pro",
				"styles": {
					"height": "844px",
					"width": "390px"
				}
			},
			"iPhone 12 Pro Max": {
				"name": "iPhone 12 Pro Max",
				"styles": {
					"height": "926px",
					"width": "428px"
				}
			},
			"iPhone 6": {
				"name": "iPhone 6",
				"styles": {
					"height": "667px",
					"width": "375px"
				}
			},
			"iPhone 6 Plus": {
				"name": "iPhone 6 Plus",
				"styles": {
					"height": "736px",
					"width": "414px"
				}
			},
			"iPhone 6s": {
				"name": "iPhone 6s",
				"styles": {
					"height": "667px",
					"width": "375px"
				}
			},
			"iPhone 6s Plus": {
				"name": "iPhone 6s Plus",
				"styles": {
					"height": "736px",
					"width": "414px"
				}
			},
			"iPhone 7": {
				"name": "iPhone 7",
				"styles": {
					"height": "667px",
					"width": "375px"
				}
			},
			"iPhone 7 Plus": {
				"name": "iPhone 7 Plus",
				"styles": {
					"height": "736px",
					"width": "414px"
				}
			},
			"iPhone 8": {
				"name": "iPhone 8",
				"styles": {
					"height": "667px",
					"width": "375px"
				}
			},
			"iPhone 8 Plus": {
				"name": "iPhone 8 Plus",
				"styles": {
					"height": "736px",
					"width": "414px"
				}
			},
			"iPhone SE": {
				"name": "iPhone SE",
				"styles": {
					"height": "379px",
					"width": "214px"
				}
			},
			"iPhone X": {
				"name": "iPhone X",
				"styles": {
					"height": "812px",
					"width": "375px"
				}
			}


    },
  },
}
