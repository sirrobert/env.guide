<<<<<<< HEAD
# env.guide

A toolkit for rapid deployment of a customized, familiar linux environment.

# Objectives

  * We want a single entrypoint to the script.  It should:
    - be able to be run from this repo (through `curl`, say)
    - take an argument for the url of a config file (in a private repo, say)

## Stack

1. Deployment
    1. **[Environment Variables](github.com/motdotla/dotenv#readme)** - Environment management
    1. **[Next.js](nextjs.org)** - Rapid and simplified web app deployment (using React).
    1. **[ESLint](eslint.org)** - Code quality assurance
    1. **[Heroku](heroku.com)** - Deployment platform (PaaS)
1. Development
    1. Components
        1. **[React](reactjs.org)** - Page building
        1. **[React Query](react-query.tanstack.com)** - Hooks for querying resources (high level)
        1. **[React Redux](react-redux.js.org)** - State management
        1. **[Storybook](storybook.js.org)** - Component development
    1. Styling
        1. **[TailwindCSS](tailwindcss.com)** - Styling (CSS) at a high level
    1. Querying Resources
        1. **[Fetch](developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)** - Remote resource querying (including API)
    1. Database
        1. **[Heroku Postgres](heroku.com/postgres)** Fully managed DB service 
1. Testing
    1. **[Jest](jestjs.io)** - Unit testing
    1. **[Cypress](www.cypress.io)** - End-to-end testing
    1. **[Testing Library](testing-library.com)** - React component testing
    1. **[Bundle Analyzer](github.com/vercel/next.js#readme)** - Bundle inspection

The base framework can be synced from the source repo with `npm run framework:upgrade`.  This can have conflicts, so be preapred.

## Scripts

Use `npm run` for these scripts.

1. Production
    1. `build` - Build the production files
    1. `start` - Start the production server
1. Development
    1. `build:analyze`    - Analyze the build bundles
    1. `build:storybook`  - Build the static storybook site
    1. `dev`              - Run the development server
    1. `storybook`        - Start the storybook server
    1. `framework:upgrade` - Update the web-app framework from the fork root
1. Testing
    1. `test`                 - Run linting, unit tests, and end-to-end tests
    1. `test:cypress.exec`    - Run the Cypress tests
    1. `test:cypress.open`    - Open the Cypress test runner
    1. `test:cypress`         - Equivalent to `cypress.open` then `cypress.run`
    1. `test:lint`            - Lint the code
    1. `test:unit.gen-output` - Generate a jest test results file
    1. `test:unit`            - Run unit tests

## Deployment to Heroku

### Setup

Follow these steps to get ready for deployment of your application.

1. Create a Heroku account, if you don't already have one, at [heroku.com]().
1. Go to your Heroku dashboard:  [dashboard.heroku.com/apps]()
1. In the upper-right corner, click the **New** button to create a new app.
    1. Just create the app.  You don't need to add it to a pipeline.
1. Once your app is created, go back to the dashboard.  You should see it
   listed.  Click to open it.

### Installing the Heroku CLI

Install the Heroku CLI on your development machine.  You can read more about it [here](https://devcenter.heroku.com/articles/heroku-cli).

For Ubuntu, use the command:

```
sudo snap install --classic heroku
```

Verify that the CLI client is installed:

```
heroku --version
```

### Login to Heroku through the CLI

Use this command to log in through your browser:

```
heroku login
```

You can read more complete instructions (and other login options) [here](https://devcenter.heroku.com/articles/heroku-cli#getting-started).

### Deploying with Git

In order to deploy to Heroku through git, you must create a Heroku *remote* in your git repository.  You can read more complete documentation [here](https://devcenter.heroku.com/articles/git#creating-a-heroku-remote).

1. Go to your app's Heroku dashboard.  Click the "Settings" tab.
1. Take note of the "App Name" at the top of the page.
1. Set the remote in your repository with
   ```
   git remote add heroku https://git.heroku.com/starfall-collective.git
   ```
   Additionally, update your `heroku:connect.remote` with the same command
   so you can restore it if you need to.
1. Whenever you like, you can deploy your app in its current state by
   pushing to the "heroku" remote:
   ```
   git push heroku master
   ```
1. *Optionally, you may detach from the build process by pressing `Ctrl+C`.
   This will not cancel the build or deployment processes.*


## Database

See the database documentation in [`./docs/DB.md`](./docs/DB.md).

## License

MIT
