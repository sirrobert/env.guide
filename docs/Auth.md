
# Auth

We use Next/Auth for authorization.  It makes a lot of the authorization tasks dead simple.

Follow this guide to set up authorization for a new web-app based app.

## Setting up your app

*This information comes from [this guide to NextAuth.js](https://next-auth.js.org/getting-started/example).*

All but one of the steps in this section are included in this source repository.  The final step requires you to add environment settings to your deployment environment.

### OAuth Providers

To set up OAuth providers (such as Github or Google), see the documentation in [docs/Auth/OAuthApps.md]() in this repository.

### Create an API endpoint (already included)

Create an API endpoint for auth.  If you have updated your framework to the most recent version, you already have this page.  This endpoint ensures that all requests to /api/auth/\* (signin, callback, signout, etc) will automatically be handed by NextAuth.js.

* `pages/api/auth/[...nextauth].js`

```
import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'

export default NextAuth({
  // Configure one or more authentication providers
  providers: [
    Providers.GitHub({
      clientId: process.env.GITHUB_ID,
      clientSecret: process.env.GITHUB_SECRET
    }),
    // ...add more providers here
  ],

  // A database is optional, but required to persist accounts in a database
  database: process.env.DATABASE_URL,
})
```

### Create a React hook (already included)

The `useSession()` React hook in the NextAuth.js client is the easiest way to check if someone is signed in.  You can use `useSession()` from anywhere in your application.  This is just an example usage.

`pages/index.js`

```
import { signIn, signOut, useSession } from 'next-auth/client'

export default function Page() {
  const [ session, loading ] = useSession()

  return <>
    {!session && <>
      Not signed in <br/>
      <button onClick={() => signIn()}>Sign in</button>
    </>}
    {session && <>
      Signed in as {session.user.email} <br/>
      <button onClick={() => signOut()}>Sign out</button>
    </>}
  </>
}
```

### Add session state (already included)

Add the following to `pages/_app.js` (if its not already included).

`pages/_app.js`

```
import { Provider as AuthProvider } from 'next-auth/client'

export default function App ({ Component, pageProps }) {
  return (
    <AuthProvider session={pageProps.session}>
      <Component {...pageProps} />
    </AuthProvider>
  )
}
```

### Deploying to production (NOT INCLUDED)

Make sure you have set the canonical URL of the website to your `NEXTAUTH_URL` in your production environment.  For Heroku, this means following these steps:

1. Go to your [heroku
   dashboard](https://dashboard.heroku.com/apps/sirrobert-web-app)
1. Click the "Settings" tab.
1. Click the "Reveal Config Vars" button.
1. Set an environment key to `NEXTAUTH_URL`.
1. Set the value for that key to your website's canonical URL, for example:
   `https://sirrobert-web-app-production.herokuapp.com`

## Creating your database schema elements

### The Database Schema

This repository already includes the default schema for NextAuth applications.  It should look like this:

```
(TODO) NOT_YET_READY
```

### Migrate the database up

## The Client API

The NextAuth.js client library makes it easy to interact with sessions from React applications.

```
{
  user: {
    name: string,
    email: string,
    image: uri
  },
  accessToken: string,
  expires: "YYYY-MM-DDTHH:mm:ss.SSSZ"
}
```

The session data returned to the client does not contain sensitive information such as the Session Token or OAuth tokens. It contains a minimal payload that includes enough data needed to display information on a page about the user who is signed in for presentation purposes (e.g name, email, image).

You can use the session callback to customize the session object returned to the client if you need to return additional data in the session object.

### Client API

NextAuth.js provides a number of helpful tools.  Read about these tools through the linked documentation pages.

#### [`useSession()`](https://next-auth.js.org/getting-started/client#usesession)

#### [`getSession()`](https://next-auth.js.org/getting-started/client#getsession)

#### [`getCsrfToken()`](https://next-auth.js.org/getting-started/client#getcsrftoken)

#### [`getProviders()`](https://next-auth.js.org/getting-started/client#getproviders)

#### [`signIn()`](https://next-auth.js.org/getting-started/client#signin)

#### [`signOut()`](https://next-auth.js.org/getting-started/client#signout)

#### [`Provider`](https://next-auth.js.org/getting-started/client#provider)

#### [Alternatives to NextAuth.js](https://next-auth.js.org/getting-started/client#alternatives)

### REST API

NextAuth.js exposes a REST API which is used by the NextAuth.js client.

`GET /api/auth/signin`

Displays the sign in page.

`POST /api/auth/signin/:provider`

Starts an OAuth signin flow for the specified provider.

The POST submission requires CSRF token from /api/auth/csrf.

`GET /api/auth/callback/:provider`

Handles returning requests from OAuth services during sign in.

For OAuth 2.0 providers that support the state option, the value of the state parameter is checked against the one that was generated when the sign in flow was started - this uses a hash of the CSRF token which MUST match for both the POST and GET calls during sign in.

`GET /api/auth/signout`

Displays the sign out page.

`POST /api/auth/signout`

Handles signing out - this is a POST submission to prevent malicious links from triggering signing a user out without their consent.

The POST submission requires CSRF token from /api/auth/csrf.

`GET /api/auth/session`

Returns client-safe session object - or an empty object if there is no session.

The contents of the session object that is returned are configurable with the session callback.

`GET /api/auth/csrf`

Returns object containing CSRF token. In NextAuth.js, CSRF protection is present on all authentication routes. It uses the "double submit cookie method", which uses a signed HttpOnly, host-only cookie.

The CSRF token returned by this endpoint must be passed as form variable named csrfToken in all POST submissions to any API endpoint.

`GET /api/auth/providers`

Returns a list of configured OAuth services and details (e.g. sign in and callback URLs) for each service.

It can be used to dynamically generate custom sign up pages and to check what callback URLs are configured for each OAuth provider that is configured.

NOTE: The default base path is `/api/auth` but it is configurable by specifying a custom path in `NEXTAUTH_URL`

e.g.

```
NEXTAUTH_URL=https://example.com/myapp/api/authentication
```

In this case, `/api/auth/signin` becomes `/myapp/api/authentication/signin`
