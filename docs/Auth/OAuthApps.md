
# OAuth Apps

To create app authorizations through OAuth providers, you will need to set
up applications with those providers.  This allows providers to ensure they
are offering secure authentication services to known applications.

## Github

Log into your github account and use the [Developers Settings
page](https://github.com/settings/developers) to create your app.  In most
cases, you will want to create one app for your development site and one for
your production site so you can use a canonical url of
`http://localhost:3000` (for example) in development.

**Development app example settings**

Application name: `The Farce (dev)`

*Something users will recognize and trust.*

Homepage URL: `http://localhost:3000`

*The full URL to your application homepage.*

Application description: `The Farce development app.` *optional*

*This is displayed to all users of your application.*

Authorization callback URL: `http://localhost:3000`
*Your application’s callback URL.*

**Production app example settings**

Application name: `The Farce`
*Something users will recognize and trust.*

Homepage URL: `http://thefarce.net`
*The full URL to your application homepage.*

Application description: *optional*
*This is displayed to all users of your application.*

Authorization callback URL: `http://thefarce.net`
*The application's callback URL*


When you create the app, you will be provided your client ID and client
secret.  Make sure to put these in your development or production
environments as appropriate.

```
# .env
GITHUB_CLIENT_ID=<enter GitHub app's client id>
GITHUB_CLIENT_SECRET=<enter GitHub app's client secret>
```

***IMPORTANT NOTE*** *When you generate your client secret it will be shown
in your browser.  You will never be able to retrieve it again, so copy it
down immediately.*


