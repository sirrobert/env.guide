
# Configuration

## Path Aliases

The methods below allow importing of modules this way:

```
import MyComponent from 'theme/my-module';
import AnotherOne  from 'components/another-one';
```

### Next.js

```
// jsconfig.js

{
  "compilerOptions": {
    "baseUrl": "./src",
    "paths": {
      "@/theme/*"      : ["theme/*"],
      "@/components/*" : ["components/*"]
    }
  }
}
```

### Storybook

```
// .storybook/main.js

webpackFinal: async (config) => {
  ...
  config.resolve.alias = {
    'theme'      : path.resolve(__dirname, '..', 'src', 'theme'),
    'components' : path.resolve(__dirname, '..', 'src', 'components'),
  };

  return config;
}
```

