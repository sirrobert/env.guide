# Database

## Create a database

1. Go to your Heroku dashboard:  [dashboard.heroku.com/apps]()
1. Click your app's entry to go to it's dashboard space.
1. Click the **Resources** tab.
1. Search for the "Heroku Postgres" add-on and install it.

## Connect to your database

Find your `DATABASE_URL` value.

1. Go to your Heroku dashboard page for your app.
1. Click the "Settings" tab.
1. Go to the "Config Vars" section and click the "Show Config Vars" button.
1. Your `DATABASE_URL` information should be displayed here.
1. Copy this value into your `./.env` file.

Your app is now ready to connect to your database.

### Getting Started

1. Set the `DATABASE_URL` in the .env file to point to your existing database.
	 If your database has no tables yet, read
	 https://pris.ly/d/getting-started]()

Before the next steps, you will need at least one table in your database.
Consider setting up authentication (see [docs/Auth.md]() for more
information.) 

