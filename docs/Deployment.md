 
# Deployment

## Connecting Heroku for deployment through git

Taken from [this article](https://devcenter.heroku.com/articles/git).

### Make sure the Heroku CLI is installed

```
sudo snap install heroku --classic
```

### Connecting the App to the Repository

You can either connect your repository to an app you created in the Heroku
dashboard, or create one from the command line.

#### Option A:  Connect to an existing Heroku app

Get the name of the app from the dashboard.  It will often be something like
`big-blue-pig`.

```
heroku git:remote -a <app-name>
```

### Option B:  Create a new Heroku app in the CLI

You can also create a new app and connect it from the command line.  The
output of the `heroku create` app will give you the app name.

```
# The output of this command will give you the app name
heroku create

# Confirm the app has been created
git remote -v

# Connect the git repo to the heroku app
heroku git:remote -a <app-name>
```

## Renaming the git remotes

You can give the remotes a convenient, memorable name.  When you first
connect a heroku app, the remote is named `heroku`.  

First, get a list of the current remotes:

```
heroku	https://git.heroku.com/secure-atoll-92135.git (fetch)
heroku	https://git.heroku.com/secure-atoll-92135.git (push)
origin	git@bitbucket.com:myawesomeapp/myawesomeapp.com.git (fetch)
origin	git@bitbucket.com:myawesomeapp/myawesomeapp.com.git (push)
```

Rename the heroku remote this way:

```
git remote rename heroku heroku-production
```

Do the same thing for production and staging.

## Renaming the Heroku branches

We want our apps to have useful names.  Typically we want one deployment for
staging and one for production.  If your app is called something like
"Purple Pig", perhaps good names would be:

* `purple-pig-staging`
* `purple-pig-production`

In git, our remote aliases will be 

* heroku-staging
* heroku-production

Once you have created your two heroku apps (through the heroku dashboard or
through the heroku cli), use these commands to set them up.

Connect the heroku apps as remotes:

```
heroku git:remote -a purple-pig-staging
heroku git:remote -a purple-pig-production
```

If your heroku apps sitll have weird names like `poogie-woogie-4819` you can
rename them.  

Use these commands to rename apps through Heroku:

```
# Get a list of git remotes.  The names shows are the local git aliases.
$ git remote
> origin
> heroku-staging
> heroku-production

# Show the remote name of which app each remote is attached to.
$ git remote show heroku-staging
> * remote heroku-staging
>   Fetch URL: https://git.heroku.com/poogie-woogie-4819.git
>   Push  URL: https://git.heroku.com/poogie-woogie-4819.git
>   ...

$ git remote show heroku-production
> big-pig-9123
> * remote heroku-production
>   Fetch URL: https://git.heroku.com/big-squirrel-9123.git
>   Push  URL: https://git.heroku.com/big-squirrel-9123.git
>   ...
```

Now rename the Heroku apps.

```
heroku apps:rename --remote heroku-production purple-pig-production
heroku apps:rename --remote heroku-staging    purple-pig-staging

# Alternately, you could identify the apps by current Heroku app name,
# though I find this less obvious.
heroku apps:rename --app poogie-woogie-4819 purple-pig-staging
heroku apps:rename --app big-squirrel-9123  purple-pig-staging
```
