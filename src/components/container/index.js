import PropTypes from 'prop-types'

export const Container = ({ children }) => {
  return <div className="min-h-screen flex flex-col">{children}</div>;
};

Container.propTypes = {
  children: PropTypes.array,
}

export default Container
