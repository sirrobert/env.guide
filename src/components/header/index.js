import React from "react";

export const Header = () => {
  return (
    <div className="text-center text-white font-bold text-2xl bg-gray-800 py-4" data-testid="container">
      env.guide
    </div>
  );
};
