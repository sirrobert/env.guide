import { useState } from 'react';
import PropTypes    from 'prop-types';
import styles       from './section.module.css'

const Section = ({
  title,
  children,
  as,
}) => {

  let [isCollapsed, setIsCollapsed] = useState(true);

  let Level = as;
  return (
    <div className={styles.section + ' ' + (isCollapsed ? styles.isCollapsed : '')}>
      <Level onClick={() => setIsCollapsed(!isCollapsed)}>{title}</Level>
      <section-body>{children}</section-body>
      <section-footer>
      </section-footer>
    </div>
  );
}

Section.propTypes = {
  title    : PropTypes.string,
  children : PropTypes.node,
  as       : PropTypes.oneOf([
    'h1',
    'h2',
  ]),
};

Section.defaultProps = {
  title          : 'Section',
  children       : null,
  as             : 'h1',
};

export default Section;
