import React from "react";
import { Button } from "../../components";

export const Main = () => {
  return (
    <div className="text-center font-light py-5 bg-gray-700">
      <div className="container mx-auto">
        <h1
          data-test="main-heading"
          className="text-white text-8xl mb-2"
        >
          env.guide
        </h1>
        <p className="text-lg text-white mb-3">
          Use Linux?  This is the fastest, easiest way to save the environment.
        </p>
         
        <Button type="button">
          <a
            data-test="docs-btn-anchor"
            href="https://pankod.github.io/superplate/"
            target="_blank"
            rel="noreferrer"
          >
            Get Started 
          </a>
        </Button>
      </div>
    </div>
  );
};
