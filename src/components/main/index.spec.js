/**
 * @jest-environment jsdom
 */
import { render } from "../../../test";
import { Main } from "./index";

describe("Main component testing with testing-library", () => {
  it("renders without crashing", () => {
    const component = render(<Main />);
    expect(component).toBeTruthy();
  });

  it("renders texts successfuly", () => {
    const { getByText } = render(<Main />);
    getByText("env.guide");
    getByText("Use Linux? This is the fastest, easiest way to save the environment.");
  });

  it("renders button successfuly", () => {
    const { getByText } = render(<Main />);
    getByText("Get Started");
  });
});
