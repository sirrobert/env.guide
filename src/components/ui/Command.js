import { useState } from 'react';
import PropTypes    from 'prop-types';
import styles       from './command.module.css';
import useClipboard from 'lib/hooks/useClipboard';

const Command = ({
  title,
  code,
  children,
}) => {

  let [explain , setExplain] = useState(false);
  let [isCopied, doCopy    ] = useClipboard(code);

  return (
    <command-block class={styles.commandBlock}>
      {!title ? null : (<command-title>{title}</command-title>)}
      <code className={isCopied ? styles.isCopied : ''}>
        {code.trim()}
        <code-controls>
          <code-explanation onClick={() => setExplain(!explain)}>
            {explain ? 'hide explanation' : 'explain'} 
          </code-explanation>
          <control-separator>|</control-separator>
          <code-copier onClick={doCopy}>
            {isCopied ? 'Copied!' : 'Copy'} 
          </code-copier>
        </code-controls>
      </code>
      {(!children || !explain) ? null : (
        <span className={styles.explanation}>{children}</span>
      )}
    </command-block>
  );
}

Command.propTypes = {
  code     : PropTypes.string,
  children : PropTypes.node,
  title    : PropTypes.string,
}

Command.defaultProps = {
  title    : '',
  code     : '',
  children : null,
}

export default Command;
