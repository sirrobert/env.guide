import PropTypes from 'prop-types';
import styles from './TextInput.module.css';

const TextInput = ({
  value,
  setter,
  placeholder = 'Type here...',
}) => {
  return (
    <input
      className={styles.textInput}
      type="text"
      value={value}
      placeholder={placeholder}
      onChange={(e) => {setter(e.target.value)}}
    />
  );
}

TextInput.propTypes = {
  value: PropTypes.string,
  setter: PropTypes.func,
  placeholder: PropTypes.string,
}

TextInput.defaultProps = {
  value: "",
  setter: () => {},
  placeholder: "",
}

export default TextInput;
