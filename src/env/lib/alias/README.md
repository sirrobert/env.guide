Add aliases here.  They should be named with only alphanumerics, hyphen, and end in
`.alias`

Examples:

    my-awesome-alias.alias
    grepper.alias

These should be grouped by related aliases.  So, for example, if you have
aliases for:

    alias ls="ls --color"
    alias ll="ls -ash"
    alias psg="ps | grep $1"

The first two can be grouped in something like

    dir-list.alias

and the third should be in

    psg.alias

This means in a personal config file, someone could use this syntax to
select only those aliases they prefer (pseudocode):

```
{
  import: {
    aliases: [
      "grepper",
      "dir-list",
    ]
  }
}
```

