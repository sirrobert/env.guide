Add functions here.  They should be named with only alphanumerics, hyphen, and end in
`.function`

Examples:

    my-awesome-function.function
    grepper.function

These should be grouped by related functions.  So, for example, if you have
functions for:

    # a helper for myfunction
    function myhelper {
      ...
    }

    # some utility function
    function myfunction {
      ...
    }

    # a function unrelated to the other two
    function unrelated {
      ...
    }


The first two functions could be grouped together in 

    myfunction.function

and the third should be in something like

    unrelated.function

This means in a personal config file, someone could use this syntax to
select only those functions they prefer (pseudocode):

```
{
  import: {
    functions: [
      "myfunction",
      "unrelated",
    ]
  }
}
```

