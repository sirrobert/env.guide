#!/usr/bin/bash

###
# array_has 
#
# Does the supplied array contain one element that matches the supplied arg?
#
array_has () {
  local ARR match="$1"
  shift
  for ARR; do [[ "$ARR" == "$match" ]] && return 0; done
  return 1
}

