
# Project Management

## Rationale

The initial reason for this repository was the difficulty of helping others
on a team while using git.  Stashing code just isn't an effective solution
(for me).

What I ended up doing was cloning the repo into a different directory and
checking out the co-workers branch.

This program evolved from that original practice as a way of allowing me to
work on multiple branches in parallel, each with their own stash (if
desired).

## Commands

### pm

The entrypoint to the Project Management command.  Use of this command by
itself prints a usage document.

#### Options

##### --help -h

Print the usage document.

### pm open <source> <profile> <repository> [<branch>]

Open a project to work on it.

**Example**

Imagine we were to use the `pm` tool to open this repository.  The
repository is located at `bitbucket.org/sirrobert/env.guide`.  In our
parlance, the url can be rewritten to use our (pseudo-)tokens:

    bitbucket.org/sirrobert/env.guide
    <source>/<profile>/<repository>

We will use this for all examples.

    # Check out the env.guide repository.
    $ pm open bitbucket sirrobert env.guide

**Options and Arguments**

  * Source.  The repository source (currently supports bitbucket and github)
  * Profile.  The 'namespace' of the project (e.g. username)
  * Repository.  The name of the repository.
  * Branch (optional).  The name of the branch to check out initially.  If
                        left blank, the repository default is checked out.

**Manual Equivalent**

Assume `ENVGUIDE_CMD__PM__PROJECTS_DIR=~/Projects`.

    $ mkdir -p $ENVGUIDE_CMD__PM__PROJECTS_DIR/.branch-pool
    $ cd $ENVGUIDE_CMD__PM__PROJECTS_DIR
    $ git clone git@bitbucket.org:sirrobert/env.guide.git .branch-pool/_source
    $ rm -rf .branch-pool/_movable
    $ cp -r .branch-pool/_source .branch-pool/_movable

At this point you have 'opened' the project.

### pm checkout <branch>

Check out a particular branch of the current project.  This command can be
used from within any subdirectory of any project.  It works by scanning *up*
the current path (towards root) looking for any directory with a
`.branch-pool` directory in it.  If found, that containing directory is
considered the "Local Project Root".  It's "Local" because it could be
within another project directory somewhere higher up in the directory tree.
This tool finds the *nearest* Project Root directory.

**Example**

    $ pm checkout mybranch

**Options and Arguments**

  * `<branch>`  Required.  The name of the branch to check out.

**Manual Equivalent**

This is not exactly what is done under the hood, but close.  The actual
implementation is nearly instantaneous.  This manual approach can take a bit
on larger repos.

Command: 

    $ pm checkout mybranch

Assume:

  * our `<project_root>` is found to be at `~/Projects/env.guide`
  * the desired <branch> is `mybranch`
  * our `./branch-pool` sources (`_movable` and `_source`) are available.

Our manual process will look like this.

    $ cd ~/Projects/env.guide
    $ mkdir -p ./branches
    $ if [ -d ./branches/mybranch ]; then
    >   cd ./branches/mybranch
    >   # We're done.
    > else
    >   mv -f .branch-pool/_movable ./branches/mybranch
    >   # Gets us ready for the next checkout.
    >   cp -r .branch-pool/_source ./branch-pool/_movable
    >   cd ./branches/mybranch
    > fi


