#!/usr/bin/bash

###
# Definitions
#
# PROJECT
#   A repository (with all of its branches and such).
#
# PROJECT ROOT 
#   A directory containing a ./branch-pool/_soure subdir that contains a
#   clone of the target repository.
#
# PROJECT SOURCE
#   The ./branch-pool/_source directory

###
# TODO
#
# - Make variables local or something.
# - Make a function to create the _movable dir: _checkout__create_movable
# - Finish the `store` function.

# This function assumes we're in a directory with .branch_pool/_source
_checkout__get_source_branches () {
  CURRENT_WORD="$1"
  cd .branch-pool/_source
  git branch --all | awk -F '/' '{print $3 "/" $4}' | sed 's|/$||g' | grep -E "^$CURRENT_WORD"
  cd ../..
}

_checkout__get_shorthand_branches () {
  CURRENT_WORD="$1"
  cd .branch-pool/_source
  git branch --all | awk -F '/' '{print $3 "/" $4}' | sed 's|/$||g' | grep -E "\-$CURRENT_WORD\-"
  cd ../..
}

_checkout__find_repo_root () {
  if [[ -d .branch-pool ]]; then
    POOL_DIR=$PWD
  elif [[ $PWD == '/' ]]; then
    cd $CURRENT_DIR
    NO_REPO_FOUND=1
    return
  else
    cd ..
    _checkout__find_repo_root 
  fi
}

###
# arrayContains
# Does the supplied array contain one element that matches the supplied arg?
#
# 
#
_checkout__arrayContains () {
  local ARR match="$1"
  shift
  for ARR; do [[ "$ARR" == "$match" ]] && return 0; done
  return 1
}

_checkout__is_branch_valid () {
	local CANDIDATE="$1"
  local BRANCHES
	mapfile -t BRANCHES < <( _checkout__get_source_branches "${CANDIDATE}" )
  _checkout__arrayContains "$CANDIDATE" "${BRANCHES[@]}"
}

_checkout__is_branch_shorthand () {
  local CANDIDATE="$1"
  local BRANCHES
	mapfile -t BRANCHES < <( _checkout__get_shorthand_branches "${CANDIDATE}" )

  if [[ ${#BRANCHES[@]} == 1 ]]; then
    echo ${BRANCHES[0]}
    return 0
  fi

  return 1
}

update_branches () {
    local START_PATH="$PWD"
  _checkout__find_repo_root 

  if [ ! -d .branch-pool/_source ]; then
    printf "Cannot find a .branch-pool/_source\n";
    return
  fi

  printf "Fetching data from remotes ... "
  # Update our base branches.
  $(
    cd .branch-pool/_source
    git fetch --prune
    cd ..
    cd _movable
    git fetch --prune
    cd ../..
  );
  printf "done.\n"

  # Return to where we started
  cd $START_PATH
}

checkout () {
  local BRANCH_NAME=$1
  local CURRENT_DIR=$PWD
  local NO_REPO_FOUND=0

  # Make sure we have colors
  source ~/.bash_shell/colorite

  # If we didn't provide a branch name, bail
  if [[ $BRANCH_NAME == '' ]]; then
    printf $(colorize yellow "[Warning]")
    printf " Please specify a branch name.\n"
    return
  fi

  # Try to redirect us to the root of this repo.
  _checkout__find_repo_root 

  # If we believe this isn't a repo subdir, then abort with a nice message.
  if [ $NO_REPO_FOUND == 1 ]; then
    printf $(colorize red "[Error]")
    printf " This does not appear to be a repository directory; the"
    printf " path does not contain a $(colorize yellow .branch-pool)"
    printf " directory anywhere.\n"
    printf "\n"
    printf "Consider using the $(colorize cyan grab_repo) command to "
    printf "re-initialize this directory repository.  Otherwise, simply "
    printf "clone the repository into:\n\n"
    printf "    $(colorize yellow '<some-dir>/.branch-pool/_source')\n\n"

    # Return to the user's starting dir so we don't startle him.
    cd $CURRENT_DIR

    return
  fi

  if [[ ! -d .branch-pool ]]; then

    if [[ ! -d .branch-pool/_source ]]; then
      printf $(colorize yellow "[Warning]")
      printf " This does appear to be a repository directory, but the "
      printf "source repository appears to be missing.\n"
      printf "\n"
      printf "Consider using the $(colorize cyan grab_repo) command to "
      printf "re-initialize this directory repository.  Otherwise, simply "
      printf "clone the repository into this directory:\n\n"
      printf "    $(colorize yellow './.branch-pool/_source')\n\n"

      # Return to the user's starting dir so we don't startle him.
      cd $CURRENT_DIR

      return
    fi

    printf $(colorize yellow "[Warning]")
    printf " This does not appear to be a repository directory.\n"
    printf "\n"
    printf "Consider using the $(colorize cyan grab_repo) command to initialize this "
    printf "directory with a repository.  Otherwise, simply clone "
    printf "the repository into this directory:\n\n" 
    printf "    $(colorize yellow './.branch-pool/_source')\n\n"

    # Return to the user's starting dir so we don't startle him.
    cd $CURRENT_DIR

    return
  fi

  _checkout__is_branch_valid "${BRANCH_NAME}"
  IS_VALID=$?

  if [[ $IS_VALID != 0 ]]; then

    SHORTHAND=$(_checkout__is_branch_shorthand "${BRANCH_NAME}")
    IS_VALID_SHORTHAND=$?

    if [[ $IS_VALID_SHORTHAND ]]; then
      BRANCH_NAME="${SHORTHAND}";
      IS_VALID="${IS_VALID_SHORTHAND}"
    fi
  fi


  ###
  # If the branch directory exists and is a valid branch, then we've already
  # checked it out (we assume).  Just move there.
  if [[ -d "./branches/$BRANCH_NAME" && $IS_VALID == 0 ]]; then
    cd "./branches/$BRANCH_NAME"
    return;

  ###
  # If the branch directory exists, but it isn't a valid branch, still go
  # there but let them know there might be something wonky.
  elif [[ -d "./branches/$BRANCH_NAME" && $IS_VALID == 1 ]]; then

    printf $(colorize yellow "[Warning]")
    printf " The requested directory exists in this code directory, but it "
    printf "does not seem to be a valid branch.  Perhaps your desired "
    printf "branch has more to it's name ...?\n"

    local BRANCHES
    mapfile -t BRANCHES < <( _checkout__get_source_branches "${BRANCH_NAME}" )

    if [ ${#BRANCHES[@]} == 0 ]; then
      return
    fi

    printf "\n"
    printf "Here are some branches that look similar.  Did you mean one of these?\n\n"
    printf "  - "
    printf "${BRANCHES[*]}\n\n" | sed 's| |\n  - |g'

    cd "./branches/$BRANCH_NAME"
    return 
  
  ###
  # In this case, the branch directory doesn't exist.  We'll try to:
  #   * validate the branch,
  #   * create the branch directory (with contents)
  #   * move to the branch directory,
  #   * check out the requested branch, and
  #   * do some background cleanup/prep
  else 
    if [ -d ./branches/_source ] && [ ! -d ./branches/_movable ]; then
      printf "... creating required files ...\n"
      cp -rf ./branches/_source ./branches/_movable
    fi

    _checkout__is_branch_valid "${BRANCH_NAME}"
    IS_VALID=$?

    if [ $IS_VALID == 1 ]; then
      printf "$(colorize red '[Error]') "
      printf " Could not find a branch named:  $(colorize yellow $BRANCH_NAME)\n"

      local BRANCHES
      mapfile -t BRANCHES < <( _checkout__get_source_branches "${BRANCH_NAME}" )

      if [ ${#BRANCHES[@]} == 0 ]; then
        return
      fi

      printf "\n"
      printf "Here are some branches that look similar.  Did you mean one of these?\n\n"
      printf "  - "
      printf "${BRANCHES[*]}\n\n" | sed 's| |\n  - |g'
      return 
    fi

    ###
    # If we got here, we have a valid branch name (meaning it was found in
    # git branch --all).  In that case, let's move foward securely.

    # Move our _movable branch to the new location.  Remember, if there was
    # already one checked out, we just went there before and exited the
    # function.  That means there's no chance of copying _movable into
    # $BRANCH_NAME.
    mkdir -p ./branches
    mv -f ./.branch-pool/_movable "./branches/$BRANCH_NAME"

    ###
    # Now we copy our _source branch over to a temporary movable file.  When
    # that's done, move it to _movable.  We do it in two steps like this to
    # make sure we never have a _movable that's only partially copied.
    # 
    # Finally, we run this in a subshell and put that shell into the
    # background so the user doesn't have to know (care) its happening.
    (
      (
        cp -rf ./.branch-pool/_source ./.branch-pool/_tmp     && \
        mv -f  ./.branch-pool/_tmp    ./.branch-pool/_movable
      ) &
    ) &>/dev/null 2>&1

    cd "./branches/$BRANCH_NAME"
    git fetch --prune
    git checkout $BRANCH_NAME
  fi
}

###
# Autocompletion.  
# 
# This is a little broken still.

# this is a custom function that provides matches for the bash autocompletion
_checkout__autocomplete () {
    # Reset the autocomplete list.
    COMPREPLY=()

    # We're going to move around during this command, so catch our start dir
    # and return there when it's time.
    local START_PATH="$PWD"

    # The first thing to do is try to find a directory in the ancestors of
    # the path that has .branch-pool in it.  The nearest one we find
    # (traversing up the tree) is assumed to be the one we want.
    _checkout__find_repo_root

    if [ ! -d .branch-pool/_source ]; then
      return
    fi

    mapfile -t BRANCHES < <( _checkout__get_source_branches "${COMP_WORDS[COMP_CWORD]}" )

    cd $START_PATH
    COMPREPLY+=("${BRANCHES[@]}")
}

###
# Specify a function to execute for expansion options.
complete -F _checkout__autocomplete checkout

###
# Specify a static wordlist for autocompletion.
# complete -W "now tomorrow never" checkout

###
# Expand the current directory for autocompletion
# complete -A directory checkout

##
# A complete list of built-in expansion options.
# https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion-Builtins.html#Programmable-Completion-Builtins
# 

#   echo ""
#   echo "COMP_WORDS : ${COMP_WORDS}"
#   echo "COMP_CWORD : ${COMP_CWORD}"
#   echo "COMP_WORDS[COMP_CWORD] : ${COMP_WORDS[COMP_CWORD]}"
#   echo "COMP_LINE : ${COMP_LINE}"
#   echo "COMP_POINT : ${COMP_POINT}"
#   echo "COMP_KEY : ${COMP_KEY}"
#   echo "COMP_TYPE : ${COMP_TYPE}"
#   echo "args : $@"
#   echo "reply : ${COMPREPLY}"

    # COMP_WORDS -  an array of all words typed after the name of the program
    #               the compspec belongs to.

    # COMP_CWORD -  an index of the COMP_WORDS array referring to the
    #               current word in selection

    # COMP_LINE  -  The current command line


###
# store
#
# Expected behaviors
#
#   ALWAYS ABORT AND WARN if a requested branch has 
#     - uncommitted code
#     - unpushed commits
#     - anything in the stash
# 
#   When a branch name is specified, move it to a temporary deletion
#   location in the branch pool directory and remove it in the background.
#   The user should be moved to the PROJECT ROOT.
#

store () {
  local BRANCH_NAME=$1
  local CURRENT_DIR=$PWD

  printf "[$(colorize cyan "Info")]"
  printf " This is a planned feature.  It is not yet implemented.\n"
  return

  # Try to put the current directory away.
  if [ "$BRANCH_NAME" == '' ]; then
    return
  fi
}


