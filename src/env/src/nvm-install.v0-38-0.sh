#!/usr/bin/env bash

# Use a default installation directory.  If the calling environment has
# ENV_GUIDE_DIR set, use that instead.
if [ "$ENV_GUIDE_DIR" != '' ]; then
  INSTALL_DIR="$ENV_GUIDE_DIR"
else
  INSTALL_DIR=~/.env.guide
fi

mkdir -p "$INSTALL_DIR"
NVM_DIR="$INSTALL_DIR/.nvm" && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash

###
# NVM already auto-detects if nvm has been added to the user's profile/path.
#
