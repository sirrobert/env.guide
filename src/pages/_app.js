import React from "react";
//import { AppProps } from "next/app";
import PropTypes from 'prop-types'
import "tailwindcss/tailwind.css";
import "../styles/global.css";
import { QueryClient, QueryClientProvider } from "react-query";
import { Hydrate } from "react-query/hydration";
import { Provider as StateProvider } from "react-redux";
import { Provider as AuthProvider } from "next-auth/client";

import store from "../redux/store";

function MyApp({ Component, pageProps }) {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={pageProps.dehydratedState}>
        <StateProvider store={store}>
          <AuthProvider session={pageProps.session}>
            <Component {...pageProps} />
          </AuthProvider>
        </StateProvider>
      </Hydrate>
    </QueryClientProvider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.any,
  pageProps: PropTypes.any,
}

export default MyApp;
