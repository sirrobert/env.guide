import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'
//import { MongoDBAdapter } from '@next-auth/mongodb-adapter';
//import clientPromise from 'lib/db/mongodb';

const DB_URL = new URL(process.env.DATABASE_URL)

export default NextAuth({
  // Configure one or more authentication providers
  providers: [
    Providers.GitHub({
      clientId: process.env.GITHUB_CLIENT_ID,
      clientSecret: process.env.GITHUB_CLIENT_SECRET,
    }),
    // ...add more providers here
  ],

  callbacks: {
    //async signIn(user, account, profile) {
    async signIn() {
      return true
    },
    //async redirect(url, baseUrl) {
    async redirect(url, baseUrl) {
      return baseUrl || url
    },
    //async session(session, user) {
    async session(session) {
      return session
    },
    //async jwt(token, user, account, profile, isNewUser) {
    async jwt(token) {
      return token
    }
	},

  // A database is optional, but required to persist accounts in a database
  //database: process.env.DATABASE_URL,
	// To work on Heroku's free tier, use this setup.
	// https://github.com/nextauthjs/next-auth/issues/822
	database: {
		// DB_URL.protocol includes the : for some reason.
		type: DB_URL.protocol.replace(':',''),
		host: DB_URL.hostname,
		port: DB_URL.port || 5432,
		username: DB_URL.username,
		password: DB_URL.password,
		database: process.env.DATABASE_URL.split(/\//g).pop(),
		ssl: true,
		extra: {
			ssl: {
				rejectUnauthorized: false,
			},
		},
	}

})
