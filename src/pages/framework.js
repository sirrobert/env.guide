import PropTypes    from 'prop-types'
import Link         from 'next/link'
import styles       from './framework.module.css'


const RequireAppName = ({name}) => {
  if (name) {
    return null;
  }

  return (
    <ui-info class={styles.warning}>
      <div>The application name has not yet been set.  You can set it at the top of the page.</div>
    </ui-info>
  );
}

RequireAppName.propTypes    = { name: PropTypes.string };
RequireAppName.defaultProps = { name: "" };

const RequireRepoName = ({name}) => {
  if (name) {
    return null;
  }

  return (
    <ui-info class={styles.warning}>
      <div>
        The repository name has not yet been set.  You can set it in Section
        2, "Fork the Repository".
      </div>
    </ui-info>
  );
}

RequireRepoName.propTypes    = { name: PropTypes.string };
RequireRepoName.defaultProps = { name: "" };

const Framework = () => {

  return (
    <framework-page id={styles._page__framework}>

      <h1>Framework</h1>
      
      <p>Get started with the framework.</p>

      <h2>Links</h2>

      <p>Use these links to navigate through the manual.</p>

      <ul>

        <li>
          <p>
            <Link href="/framework/quickstart"><a>Quickstart</a></Link>
            Get started quickly using the default setup.
          </p>
        </li>

        <li>
          <p>
            <Link href="/framework/deploy/heroku"><a>Heroku Deployment</a></Link>
            Deploy quickly and easily with Heroku.
          </p>
        </li>

        <li>
          <p>
            <Link href="/framework/db"><a>Databases</a></Link>
            Find out about the built-in database options.
          </p>

          <ul>
            <li>
              <p>
                <Link href="/framework/db/postgresql"><a>PostgreSQL</a></Link>
                Learn how to integrate PostgreSQL.
              </p>
            </li>
            <li>
              <p>
                <Link href="/framework/db/mongo-db"><a>MongoDB</a></Link>
                Set up MongoDB as your database.
              </p>
            </li>
            <li>
              <p>
                <Link href="/framework/db/mysql"><a>MySQL</a></Link>
                Connect MySQL to your app.
              </p>
            </li>
          </ul>
        </li>

        <li>
          <p>
            <Link href="/framework/auth"><a>Authentication</a></Link>
            Set up authentication quickly and easily.
          </p>
        </li>

      </ul>

    </framework-page>
  );
}

export default Framework;
