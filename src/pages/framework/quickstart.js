import { useState } from 'react';
import PropTypes    from 'prop-types';
import Section      from 'components/layout/Section'
import TextInput    from 'components/ui/input/TextInput'
import styles       from './quickstart.module.css';
import Command      from 'components/ui/Command';

const RequireAppName = ({name}) => {
  if (name) {
    return null;
  }

  return (
    <ui-info class={styles.warning}>
      <div>The application name has not yet been set.  You can set it at the top of the page.</div>
    </ui-info>
  );
}

RequireAppName.propTypes    = { name: PropTypes.string };
RequireAppName.defaultProps = { name: "" };

const RequireRepoName = ({name}) => {
  if (name) {
    return null;
  }

  return (
    <ui-info class={styles.warning}>
      <div>
        The repository name has not yet been set.  You can set it in Section
        2, "Fork the Repository".
      </div>
    </ui-info>
  );
}

RequireRepoName.propTypes    = { name: PropTypes.string };
RequireRepoName.defaultProps = { name: "" };

const Quickstart = () => {

  let [appName        , setAppName        ] = useState('');
  let [repoName       , setRepoName       ] = useState('');

  const scrubAppName  = (text) => setAppName(
    text.trim().replace(/\W+/g, '-').toLowerCase()
  );

  const scrubRepoName = (text) => setRepoName(
    text.trim().replace(/[^\w.\-_]+/g, '-').toLowerCase()
  );

  const getUsableName = () => appName.replace(/\W/, '').toLowerCase();

  return (
    <quickstart-page id={styles._page__quickstart}>

      <Section title={`Choose an application name (${appName || 'set the app name'})`}>
        <p>
          Choose a name for your application.  It should contain only the
          characters a-z, A-Z, 0-9, and hyphens.
        </p>
        Application Name: <TextInput value={appName} setter={scrubAppName}/>
      </Section>

      <Section title={`Fork the Repository (${repoName || 'set the repo name'})`}>
        <ol>
          <li>Go to <a href="https://bitbucket.org/sirrobert/web-app/src/master">the
            repo's page</a> on bitbucket.
          </li>
          <li>Click the three dots at the upper-right of the page <dots-menu/></li>
          <li>Click the "Fork this repository" option.</li>
          <li>
            On the next page, choose your repository options (workspace,
            project, repo name, etc.
          </li>
          <li>
            <p>
              Enter the repository name you chose here.  It should contain
              only the characters a-z, A-Z, 0-9, hyphen, underscore, and
              period. Make sure to copy it exactly.
            </p>
            <p>
              Repository Name: <TextInput value={repoName} setter={scrubRepoName}/>
            </p>
          </li>
        </ol>

      </Section>

      <Section title="Set up">

        <Section as="h2" title="Create a Heroku Account" >
          <p>
            The first step in creating Heroku apps is to create a Heroku
            account.  If you do not already have a Heroku account,
            <a href="https://dashboard.heroku.com">create one now</a>.
          </p>
        </Section>

        <Section as="h2" title="Install the Heroku CLI">
          <p>
            The Heroku CLI is a robust tool for interacting with the
            service.  The rest of this quickstart assumes that you'll be
            using the CLI, though each of the tasks can also be achieved
            through the heroku dashboard.
          </p>

          <Command 
            title="Install the Heroku CLI (Ubuntu)"
            code="sudo snap install heroku --classic"
          >
            Installs the Heroku CLI.  This command is specific to Ubuntu.
            For other distributions, see the 
            <a href="https://devcenter.heroku.com/articles/heroku-cli">
              Heroku CLI documentation
            </a>
          </Command>

          <Command title="Verify the installation" code="heroku --version">
            Verifies the CLI installation.
          </Command>
        </Section>

        {/*******************************/}
        <Section as="h2" title="Login to Heroku">
          <p>
            Login to the Heroku CLI.  It has a good interface through the browser.
          </p>

          <Command title="Log in to Heroku" code="heroku login">
            <p>
	            This command will open a browser window to a heroku login url.
  	          Just log in as normal and the CLI will respond automatically.
            </p>

            <p>
              You can read more complete instructions (and other login options) 
              <a href="https://devcenter.heroku.com/">here</a>.
            </p>
          </Command>
        </Section>
      </Section>


      <Section title="Create Heroku Apps"
        
      >
        {/*******************************/}
        <Section as="h2" title="Create Remotes">
          <RequireAppName name={appName}/>

          <p>
            You will need two deployment remotes (in addition to your
            repository's normal code repository remote).
          </p>

          <p>
            Make sure you have set the app name at the top of this page.  It
            will automatically update the commands below.
          </p>

          <Command
            title="Create the Staging App"
            code={`heroku apps:create heroku-staging --remote ${getUsableName()}-staging`}
          >
            <p>
              Creates a new app named <code>{getUsableName()}-staging</code>.  The
              alias of the remote (locally) will be <code>heroku-staging</code>.
            </p>
          </Command>

          <Command
            title="Create the Production App"
            code={`heroku apps:create heroku-production --remote ${getUsableName()}-production`}
          >
            <p>
              Creates a new app named <code>{getUsableName()}-production</code>.  The
              alias of the remote (locally) will be <code>heroku-production</code>.
            </p>
          </Command>
        </Section>


      </Section>

      <Section title="Create Databases">
        <p>
          Your heroku application should have a database for production and
          a database for staging.  This allows you to do testing on your
          staging database without affecting your live deployments.
        </p>

        <p>
          You should have three databases by the end of this process:
        </p>

        <ul>
          <li>Development Database</li>
          <li>Staging Database</li>
          <li>Production Database</li>
        </ul>

        <p>
          These are our canonical pipeline stages.  Do your work in
          development.  When it's ready, push it to staging.  When that's
          approved, push it to production.  Remember that your development
          db is used only by you.  The staging db is shared by the team, and
          the production db is shared by the world.  That mental framework
          helps us determine the impact of our changes, and shows why we
          want three stages (minimum).
        </p>

        <Section as="h2" title="Create the Databases">
          <RequireAppName name={appName}/>

          <Command code={`heroku addons:create heroku-postgresql:hobby-dev --name ${getUsableName()}-production-db`}>
            Create the primary production database with a sensible name that
            matches your app name.
          </Command>

          <Command code={`heroku addons:create heroku-postgresql:hobby-dev --name ${getUsableName()}-staging-db`}>
            Create the primary staging database with a sensible name that
            matches your app name.
          </Command>

          <p>
            You can create your development database on your local machine using `psql`.
          </p>

          <Command code={`psql`}>Open the PSQL postgres query shell.</Command>
          <Command code={`CREATE DATABASE ${getUsableName()}`}>Create your database locally.</Command>
          <Command code={`GRANT ALL PRIVILEGES ON DATABASE '${getUsableName()}.* TO '${getUsableName()}'@'localhost' IDENTIFIED BY ${getUsableName()};`}>
            Grant all privileges on the database
          </Command>

        </Section>

        <Section as="h2" title="Set up your Database Environment">
          <RequireAppName name={appName}/>

          <p>
            Once your application has been set up and the databases created,
            you need to make your application aware of them.  Use the
            following commands to retrieve your database URLs and add them
            to your local environment.
          </p>

          <p>
            First, go to your repository's root directory.  Then use the
            following commands.  Note that this sets your development
            environment database to your staging database.
          </p>

          <Command code={`echo DATABASE_URL=$(heroku config:get DATABASE_URL -a ${getUsableName()}-staging-db) >> .env`}>
            This connects to Heroku and gets the DATABASE_URL for your
            staging database.  It stored that value in the .env file.
          </Command>

        </Section>
      </Section>

      <Section title="Authentication">
        <p>
          <em>
            NOTE: This section does not cover the setting up of OAuth apps
            with OAuth providers.  For that, please see the file
            ./docs/Auth/OAuthApps.md in the repository.
          </em>
        </p>

        <p>
          Your database schema (in `./database/schema/*.psql`) should be
          populated with the database tables used by NextAuth for
          authenticating through OAuth apps.
        </p>

        <Section as="h2" title="Set the NEXTAUTH_URL Environment Variable">
          <p>
            Set the `NEXTAUTH_URL` environment variable in your `.env` file.
            It should be set to the canonical URL of your website.
          </p>

          <Command code={`echo NEXTAUTH_URL=<canonical-url>`}>
            Set the NEXTAUTH_URL environment variable.  Examples of a canonical URL are
            `https://example.com/myapp/api/authentication'.
          </Command>
        </Section>


      </Section>

    </quickstart-page>
  );
}

export default Quickstart;
