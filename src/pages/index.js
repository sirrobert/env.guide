import React from "react";
//import { signIn, signOut, useSession } from 'next-auth/client'

import { Container, Header, Main, Footer, Cards } from "components";

const Home = () => {
  //const [ session, loading ] = useSession()

  return (
    <Container>
      <Header />
      {/* This is where we do auth
			{
				!session && <>
					Not signed in <br/>
					<button onClick={() => signIn()}>Sign in</button>
				</>
			}
			{
				session && <>
					Signed in as {session.user.email} <br/>
					<button onClick={() => signOut()}>Sign out</button>
				</>
			}
      */}
      <Main />
      <Cards />
      <Footer />
    </Container>
  )
};

export default Home;
