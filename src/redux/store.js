import { configureStore } from "@reduxjs/toolkit";
import { useDispatch }    from "react-redux";
import rootReducer        from "./reducers";

export const defaultInitialState = {};

/**
 * createStore
 *
 * Export a store creation mechanism.  The advantage of exporting this
 * function is that we can create stores aside from our main store.  This is
 * useful for loading fixtures, for example, in storybook stories.
 *
 * @param {any} initialState - The initial state of the store.
 * @returns {store} A redux store initialized with the provided initial
 *                  state or an empty object.
 */
export const createStore = (initialState=defaultInitialState) => configureStore({
  reducer       : rootReducer,
  preloadedState : initialState,
});

//export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch();

const store = createStore();
export default store;
