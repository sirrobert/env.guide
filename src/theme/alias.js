/**
 * This module is a simple catalog of layout aliases.  When we find a set of
 * commonly-grouped style atoms, simply add them here with an appropriate,
 * semantic name.  Then use them in your components as needed.
 *
 * Please prefer a more readable layout for maintainability and add comments
 * as needed to clarify the intent.
 */

const semantics = {
  alias: {},

  grid: {
    /* This grid definition breaks according to the theme breakpoints with a
     * mobile-first bias (1 column on tiny screens).  Child-nodes of this
     * element will automatically break at the right spots.
     *
     * Don't forget to use col-span-{x} values to create multi-column items.
     */
    pageLayoutFlex: `
      grid
         grid-cols-1              gap-layout-xs      m-grid-layout-xs
      xs:grid-cols-layout-xs   xs:gap-layout-xs   xs:m-grid-layout-xs
      sm:grid-cols-layout-sm   sm:gap-layout-sm   sm:m-grid-layout-sm
      md:grid-cols-layout-md   md:gap-layout-md   md:m-grid-layout-md
      lg:grid-cols-layout-lg   lg:gap-layout-lg   lg:m-grid-layout-lg
      xl:grid-cols-layout-xl   xl:gap-layout-xl   xl:m-grid-layout-xl
    `,

    item: {
      'col-span:xs1/1-sm1/1-md1/2-lg1/3-xl1/4': `
        col-span-1
        xs:col-span-4
        sm:col-span-3
        md:col-span-4
        lg:col-span-3
        xl:col-span-4
      `,
    },

    imageGalleryControls: 'grid grid-cols-x3-flex-x1',
  },

  // Create our designed border aliases.
  border: {
    // This is an example from the design repo.  It is the border used in
    // the <hr/> element as the 'border-top' rule.
    'border-t-hr': `
      border-t-width-100
      border-t-solid
      border-t-gray-000
      border-t-opacity-20
      rounded-t-none
    `,
  },

};

/**
 * alias(dest,src)
 * @param {string} name - The name of the alias to create.
 * @param {any} source - The thing to alias (typically a css class string).
 * @returns null
 * @example
 *    alias("my-layout-item", theme.grid.item['col-span:xs1/1-sm1/1-md1/2-lg1/3-xl1/4']);
 *
 *    Now you can use the alias like so:
 *    <div className={theme.grid.pageLayoutFlex}>
 *      <div className={theme.alias['my-layout-item']}>...</div>
 *      <div className={theme.alias['my-layout-item']}>...</div>
 *    </div>
 *
 * Note that we're enclosing over `semantics`.  This is not *quite* the
 * right way to do this, but I don't yet know what the right way is.  This
 * will give us a very easy migration path whatever-it-is when we find it.
 */
export const alias = (name, source) => {
  semantics.alias[name] = source;
};

export default semantics;
