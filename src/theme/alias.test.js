/**
 * @jest-environment jsdom
 */
import theme, { alias } from './alias';

test('something', () => {
  alias('myStyle', theme.grid.item);
  expect(theme.alias.myStyle).toBe(theme.grid.item);
});
