import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import hexsorter from 'hexsorter';
import { textForBgColor } from 'theme/util';

const ColorBar = ({ value, aliases }) => {
  let [copied, setCopied] = useState(null);

  // When the "copied" changes, create a 2 second timer to clear it away.
  useEffect(() => {
    let clear = setTimeout(() => setCopied(null), 2000);
    return () => clearTimeout(clear);
  }, [copied]);

  let color = textForBgColor(value);

  let names = aliases.map((text) => (
    <li key={text}>{text}</li>
  ));

  const copyValueToClipboard = () => {
    navigator.clipboard.writeText(aliases[0]);
    setCopied('(copied)');
  };

  return (
    <button
      className={`bg-${aliases[0]} p-3 text-white w-full`}
      style={{ color }}
      onClick={copyValueToClipboard}
      onKeyDown={copyValueToClipboard}
      type="button"
    >
      {value}
      {' '}
      {copied}
      <ul className="italic">{names}</ul>
    </button>
  );
};

ColorBar.propTypes = {
  value: PropTypes.string.isRequired,
  aliases: PropTypes.arrayOf(String).isRequired,
};

const Palette = ({ name, list, selector }) => {
  console.log(name, list, selector);

  /* Given our list of colors, lets sort them by value and keep all keys as
   * aliases.  This lets us put a nice, friendly interface to them.
   */
  let index = {};
  Object.entries(list)
    .forEach(([key, value]) => {
      if (!index[value]) {
        index[value] = [];
      }
      index[value].push(`${selector}-${key}`);
    });

  // Now `index` is an index of colors by hex value with a list of aliases
  // that can be used to access that hex value.  It's an inversion of the
  // way its defined in the palette (for display purposes, these are
  // opposites).
  let bars = hexsorter
    .sortColors(
      Object.keys(index),
      'mostBrightColor'
    )
    .map((hex) => (
      <ColorBar key={hex} value={hex} aliases={index[hex].sort().reverse()}/>
    ));

  return (
    <div className="w-auto w-3/12">
      {name}
      {bars}
    </div>
  );
};

Palette.propTypes = {
  name     : PropTypes.string.isRequired,
  list     : PropTypes.arrayOf(Object).isRequired,
  selector : PropTypes.string.isRequired,
};

export default Palette;
