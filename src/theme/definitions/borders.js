/**
 * Various border widths.
 */
const scale = {
  width: {
    // Canonical border widths scaled from 000 - 999
  },

};

const alias = {
  width: {
    // Style-semantic aliases for the scaled values.
    // "none"  : standard.width['000'],
    // "thin"  : standard.width['100'],
    // "thick" : standard.width['200'],
  },
};

module.exports = {
  scale,
  alias,
};
