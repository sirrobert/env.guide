const scale = {
  /**
   * These rules get applied as the following @media patterns:
   *    @media (min-width: <value>) { ... }
   *
   * This is a "mobile-first" approach.  The idea here is that the *default*
   * styles apply to the page, then are over-written when a more
   * specific media rule applies.
   *
   * In this case, that means that the default styles apply to all cases,
   *  - but if the width is at least 320, use xs
   *  - but if the width is at least 600, use sm
   *  - ...
   *  - but if the width is at least 1140, use xl
   *
   * The implication of this is that everything above xl is accounted for
   * by xl, and everything *unaccounted-for* (in this case, devices under
   * 320) is the general, default style.  Hence "mobile-first".
   *
   * IMPORTANT NOTE:  Order matters here because of the way CSS @media
   * rules cascade.  When using desktop-first, do largest-to-smallest.
   * When using mobile-first, do smallest-to-largest.  Tailwind respects
   * this.
   */
  xs: '320px',
  sm: '600px',
  md: '768px',
  lg: '1000px',
  xl: '1140px',
};

// Our aliasing for this doesn't include mobile because we're
// "mobile-first", which means default values are interpreted as mobile.  We
// *override* defaults with these specifics.
//
// For example, using just these two aliases in a component will only set
// breakpoints at sm and lg, and default to mobile:
//
//    <div className='tablet:m-thin desktop:m-thick m-none'>
//      ...
//    </div>
//
const aliases = {
  // Style-semantic aliases for scale values
  // tablet  : scale.sm,
  // desktop : scale.lg,
  // feature : scale.xl,
};

module.exports = {
  scale,
  aliases,
};
