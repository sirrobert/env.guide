/**
 * The naming in this file is inconsistent with other files.  This is
 * because this definition in intrinsically complex.  It's not a simple
 * scale, it's got multiple factors involved that don't fit on a scale.
 */
const standard = {
  /**
   * Definitions of grid columns by style-sematic names.  Note that this
   * property is rendered by tailwind as grid-cols-${name}.
   */
  columns: {
    /**
     * Use as, for example,
     *    grid-cols-layout-xs
     *    sm:grid-cols-layout-sm
     */
    // 'layout-xs' : 'repeat(2,  1fr)',
    // 'layout-sm' : 'repeat(4,  1fr)',
    // 'layout-md' : 'repeat(6,  1fr)',
    // 'layout-lg' : 'repeat(10, 1fr)',
    // 'layout-xl' : 'repeat(14, 1fr)',

    // An example of what could be a commonly-used grid for components.
    // 'x2-flex-x2': 'repeat(2, 100px) 1fr 50px 2rem',
  },

  /**
   * Definitions of grid gaps by style-semantic names.
   */
  gap: {
    /**
     * Use as, for example,
     *    gap-layout-xs
     *    md:gap-layout-md
     */
    // 'layout-xs': '0.8000rem',
    // 'layout-sm': '0.9500rem',
    // 'layout-md': '1.0000rem',
    // 'layout-lg': '1.4000rem',
    // 'layout-xl': '1.8500rem',
  },

  /**
   * Definitions of grid margins by style-semantic names.
   */
  margin: {
    // 'grid-layout-xs': '0.7800rem',
    // 'grid-layout-sm': '0.7800rem',
    // 'grid-layout-md': '1.1000rem',
    // 'grid-layout-lg': '1.3000rem',
    // 'grid-layout-xl': '1.3000rem',
  }
};

const aliases = {
};

module.exports = {
  standard,
  aliases
};
