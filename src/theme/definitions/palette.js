const scale = {
  /**
   * These scale values can be described in a variety of ways.  One common
   * approach is 
   *  {
   *    brand: {
   *      primary: {
   *        "000" : "", // lightest version
   *        ...
   *        "900" : "", // darkest version
   *      },
   *      secondary: { ... }
   *      yellow: { ... }
   *    },
   *
   *    orange: { ... },
   *    ...
   *  }
   */
};

const alias = {
  /**
   * It's a good idea to provide a couple of versions of each color using
   * style-semantic aliases.  Good standard aliases include:
   *
   *    ...
   *    light-x2
   *    light
   *    default
   *    dark
   *    dark-x2
   *    ...
   *
   *  Examples may include:
   *
   *    brand: {
   *      primary: {
   *        light   : scale.brand.primary['300'],
   *        default : scale.brand.primary['500'],
   *        dark    : scale.brand.primary['700'],
   *      },
   *    },
   *
   *
   *  I'm not a huge fan of extreme abbreviation, though it's nice if we can
   *  get tokens of the same length.
   *
   *  Aliasing can also be done in a way that include opacity.
   */
};

module.exports = {
  scale,
  alias,
};
