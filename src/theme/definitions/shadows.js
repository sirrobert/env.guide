/**
 * Shadows are compound units.  They have size and color.  I'm not sure this
 * is the best way to do this, but here we are for now.
 */
const scale = {
  /**
   * Because box shadows are intrinsically composite style rules, we define
   * the atoms here, but we don't actually use them in the theme.  Instead,
   * we create aliases below and extend tailwind's box-shadow styles with
   * those.  That lets us keep atomic control and semantic definitions.
   */
  boxShadow: {
    size: {
      // scale values here
    },
    
    /**
     * I think this should somehow use colors and alpha from palette.js
     */
    color: {
    }
  }
};

/**
 * Create box-shadow definitions here.  Don't try to use the sizing and
 * color atoms from above directly as styles.  Use Storybook for an
 * interactive guide.
 *
 * The css rule looks like this:
 *
 *    box-shadow: 5 2 2 rgba(0,0,0,0.5);
 * 
 * which is 
 *
 *    box-shadow: <spread> <x-offset> <y-offset> <color>;
 *
 * A good example of aliases may be:
 *
 *    "card" : [
 *      scale.boxShadow.size['500'],
 *      scale.boxShadow.size['200'],
 *      scale.boxShadow.size['500'],
 *      scale.boxShadow.color['100'],
 *    ].join(' '),
 *
 */
const alias = {
};

module.exports = {
  // We don't want to export these atomic values, just aliases.
  scale,
  alias,
};
