/**
 * Spacings should be standard for width, height, max-width, max-height,
 * padding, etc.  Tailwind comes with some good defaults, so we extend them
 * here.
 *
 * Note that we can have negative values as well.
 */
const scale = {
  // "000": "-5.0000rem"
  // "500": " 0.0000rem"
  // "900": " 5.0000rem"
};

/**
 * Style-sematic aliasies for common sizes.
 */
const alias = {
};

module.exports = {
  scale,
  alias,
};
