/**
 * Spacings refer to margins and padding and such.  Tailwind comes with some
 * good values for this.  We can extend them here.
 */
const scale = {
};

/**
 * These are just example aliases (that are also basically sensible).  We
 * could alias the standard values with things like:
 *
 *  margin: {
 *    ...
 *    "thin"  : standard.margin['200'],
 *    "thick" : standard.margin['700'],
 *    ...
 *    }
 *
 */
const alias = {
  // margin: {
  //   "none": standard.margin['000'],
  //   "full": standard.margin['900'],
  // },
  //
  // padding: {
  //   "none": standard.padding['000'],
  //   "full": standard.padding['900'],
  // },
}

module.exports = {
  scale,
  alias,
};

