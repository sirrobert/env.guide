const scale = {
  /**
   * In all the font-family definitions below, the `ui-` prefixed versions
   * are included but commented out for now.  There's a bit of nuance about
   * these, but the basic idea is they allow users to define their own
   * higher-priority fonts if the family matches (for accessibility).  It's
   * not widely supported right now, but is a coming standard.  It's also
   * super easy for users to stumble over if they change their fonts without
   * knowing all the implications.
   *
   * We can always turn this on when there's greater user-adoption (which
   * means herd awareness).
   */

  /**
   * Use these like this:
   *  font-serif
   *  font-sans
   *  font-condensed
   *
   * Tailwind automatically handles the array correctly, but it *does not*
   * try to handle quoting or escaping.
   */
  fontFamily: {
    condensed : [],
    mono      : [],
    sans      : [],
    serif     : [
      // 'ui-serif',
      // "'Times New Roman'",
    ],
  },

  fontSize: {},

  // Obvious and standard.  There's nothing else to do here.
  fontWeight: {
    "000":"000",
    "100":"100",
    "200":"200",
    "300":"300",
    "400":"400",
    "500":"500",
    "600":"600",
    "700":"700",
    "800":"800",
    "900":"900",
  },

  /**
   * Line height is a unit relative to font-size.  It's a simple
   * multiplicative, so line-height of 1.4 means the line should be
   * `(font-size * 1.4)`.
   *
   * Line heights can also be expressed with other units, but don't.
   */

  lineHeight         : {},
	letterSpacing      : {},
  fontSmoothing      : {},
  fontStyle          : {},
  fontVariantNumeric : {},
};

const alias = {
  letterSpacing : {
    //'tight-x2'  : standard.letterSpacing['-500'],
    //'tight'     : standard.letterSpacing['-400'],
    //'default'   : standard.letterSpacing['-300'],
    //'wide'      : standard.letterSpacing['-200'],
    //'wide-x2'   : standard.letterSpacing['-100'],
  },

  fontSize: {
    //'2xs'     : standard.fontSize['200'],
    //'xs'      : standard.fontSize['200'],
    //'sm'      : standard.fontSize['300'],
    //'default' : standard.fontSize['400'],
    //'lg'      : standard.fontSize['600'],
    //'xl'      : standard.fontSize['700'],
    //'2xl'     : standard.fontSize['800'],
  },

  /**
   * Use tailwind defaults for font-weight.  Listed here for quick
   * reference.
  fontWeight: {
    thin,
    extralight,
    light,
    normal,
    medium,
    semibold,
    bold,
    extrabold,
    black,
  },
   */

  lineHeight: {
    //'none'     : standard.lineHeight['100'],
    //'2xs'      : standard.lineHeight['200'],
    //'xs'       : standard.lineHeight['300'],
    //'sm'       : standard.lineHeight['400'],
    //'standard' : standard.lineHeight['500'],
    //'lg'       : standard.lineHeight['600'],
    //'xl'       : standard.lineHeight['700'],
    //'2xl'      : standard.lineHeight['800'],
    //'3xl'      : standard.lineHeight['900'],
  }
  
};

module.exports = {
  scale,
  alias,
};
