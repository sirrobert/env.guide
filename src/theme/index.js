const deepmerge = require('deepmerge');

// Used for example right now
const colors = require('tailwindcss/colors');

/**
 * Our theme definitions.  The theme "override" object is created here and
 * then loaded into the app through tailwind.config.js in the root of the
 * dir (check webpack for precisely how it gets loaded).
 */
//const colors      = require('./colors');
//const breakpoints = require('./breakpoints');
//const grid        = require('./grid');
//const spacing     = require('./spacing');
//const sizing      = require('./sizing');
//const shadows     = require('./shadows');
//const borders     = require('./borders');
//const typography  = require('./typography');

/**
 * These get hoisted because they're declarations with the function keyword.
 * They are not exported because they're of no use outside this file... the
 * whole file is pre-processed for CSS compilation and is not actively used
 * in the app, aside through the classnames interface.
 */

/**
 * Replace a component of the tailwind theme with the data provided.  This
 * completely removes the tailwind defaults and replaces them with only the
 * data provided.
 *
 * Note that this stacks-- if you replace() something that's already been
 * replaced, it keeps the old info and adds your new info to it.  Later
 * values overwrite existing former values in conflicts.
 *
 * @param {object} theme - The local theme object.
 * @param {string} name - The name of the tailwind theme section to replace.
 * @param {object} data - A hash of data in tailwind theme config format.
 * @returns {void}
 * @example 
 *    replace(theme, 'colors', { 'black': '#000000', ... })
 */
function replace (theme, name, data) {
  if (!data) {
    return;
  }

  if (!theme[name]) {
    theme[name] = {};
  }

  theme[name] = deepmerge(theme[name], data);
}

/**
 * Extend a component of the tailwind theme with the data provided.
 * @param {object} theme - The local theme object.
 * @param {string} name - The name of the tailwind theme section to replace.
 * @param {object} data - A hash of data in tailwind theme config format.
 * @returns {void}
 * @example 
 *    extend(theme, 'colors', { 'black': '#000000', ... })
 */
function extend (theme, name, data) {
  if (!data) {
    return;
  }

  if (!theme.extend) {
    theme.extend = {};
  }

  if (!theme.extend[name]) {
    theme.extend[name] = {};
  }

  theme.extend[name] = deepmerge(theme.extend[name], data);
}

/**
 * We do this declaratively so we don't have object merge problems if one
 * "theme file" defines, for example, some things to replace and some things
 * to extend.
 *
 * IMPORTANT:  This is *NOT* the place to make aggregations of theme
 * elements.  This is where we make atomic aliases, such as referring to
 * '#ffffff' as 'white' or '10rem' as 'large'.  Making changes here affects
 * the lowest-level theme atoms.  So, extending margin to include "thin"
 * here will add:
 *
 *  m-thin
 *  mx-thin
 *  my-thin
 *  ...
 *
 * If you want to create site-semantic aliases in javascript (rather than
 * style-semantic aliases in CSS), check out the ./alias.js file.
 */
// Define our local theme container.
const theme = {};
replace(theme, "colors", colors);
extend (theme, "colors", colors);

// These are examples of how you could include them.

// Palette
// replace(theme, 'colors' , colors.scale);
// extend (theme, 'colors' , colors.alias);

// Screens/Breakpoints
// replace(theme, 'screens', breakpoints.scale);
// extend (theme, 'screens', breakpoints.alias);

// Grids 
// replace(theme, 'gridTemplateColumns', grid.standard.columns);
// extend (theme, 'gridTemplateColumns', grid.alias);
// extend (theme, 'gap'   , grid.standared.gap);
// extend (theme, 'margin', grid.standard.margin);

// Spacings
// extend(theme, 'margin' , spacing.scale.margin);
// extend(theme, 'margin' , spacing.alias.margin);
// extend(theme, 'padding', spacing.scale.padding);
// extend(theme, 'padding', spacing.alias.padding);

// Sizings (width/height)
// extend(theme, 'height'   , sizing.scale.height);
// extend(theme, 'minHeight', sizing.scale.minHeight);
// extend(theme, 'maxHeight', sizing.scale.maxHeight);
// extend(theme, 'width'    , sizing.scale.width);
// extend(theme, 'minWidth' , sizing.scale.minWidth);
// extend(theme, 'maxWidth' , sizing.scale.maxWidth);

// Box shadows
// extend(theme, 'boxShadow', shadows.alias);

// Typography
// replace(theme, 'fontFamily'   , typography.scale.fontFamily);
// replace(theme, 'fontSize'     , typography.alias.fontSize);
// replace(theme, 'letterSpacing', typography.alias.letterSpacing);
// extend (theme, 'lineHeight'   , typography.alias.lineHeight);

/**
 * Borders
 *
 * Tailwind already comes with all the default styles required by our design
 * spec, except for some of the border-widths.  We'll need some custom
 * aliasing to get our mix-and-match right, but that's not done here (check
 * theme/tailwind/alias.js)
 */
// replace(theme, 'borderWidth', borders.scale.width);
// extend (theme, 'borderWidth', borders.alias.width);

// This gets consumed by tailwind.config.js
module.exports = theme;
