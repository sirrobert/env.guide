/**
 * A theme utility module
 *
 * @module theme/util
 */

/**
 * Convert pixels to rem ('root em').
 * @param {number} pixels - The size in pixels
 * @param {number} base - An base unit size in pixels (e.g. 16)
 * @return {string} A CSS rem unit (e.g. '1.25rem')
 * @example pxToRem(40,16)  // '2.5rem'
 */
export const pxToRem = (px, base) => `${(px / base).toFixed(4)}rem`;

/**
 * Normalize a string to a font name
 * @param {string} name - The name of a font
 * @return {string} The normalized name (in quotes if needed)
 * @example
 *
 *    normalizeFontName('sans-serif')        // 'sans-serif'
 *    normalizeFontName('Helvetica')         // 'Helvetica'
 *    normalizeFontName('Helvetica Neue')    // '"Helvetica Neue"'
 *    normalizeFontName('.SFNSText-Regular') // '".SFNSText-Regular"'
 *
 * Note that the [CSS
 * spec](https://www.w3.org/TR/2018/REC-css-fonts-3-20180920/#propdef-font-family)
 * says it this way:
 *
 *    To avoid mistakes in escaping, it is recommended to quote font family
 *    names that contain white space, digits, or punctuation characters
 *    other than hyphens.
 *
 * And also a special list of reserved keywords that ought not to be quoted,
 * even though they may contain other chars:
 *
 *    The following generic family keywords are defined: ‘serif’,
 *    ‘sans-serif’, ‘cursive’, ‘fantasy’, and ‘monospace’. These keywords
 *    can be used as a general fallback mechanism when an author's desired
 *    font choices are not available. As keywords, they must not be quoted.
 *    Authors are encouraged to append a generic font family as a last
 *    alternative for improved robustness.
 */

export const normalizeFontName = (name) => {
  if (name.match(/^[\w-]+$/g)
   || name === 'serif'
   || name === 'sans-serif'
   || name === 'cursive'
   || name === 'fantasy'
   || name === 'monospace') {
    return name;
  }
  return `"${name}"`;
};

/**
 * Merge font strings into an acceptable CSS font string.
 * @param {...string} list - Any combination of strings or arrays of strings
 *                           that represent css font names.
 *
 * @example
 *
 *    > fonts('Helvetica', 'Helvetica Neue')
 *    'Helvetica, "Helvetica Neue"'
 *    > fonts('Helvetica', 'sans-serif')
 *    'Helvetica, sans-serif'
 */
export const renderFontFamilyStr = (...list) => {
  list = list.flat(10);
  return list
    .flat(10)
    .map((name) => normalizeFontName(name))
    .join(', ');
};

/**
 * Flatten an object's selectors with optional logic about what to flatten.
 * @param {object} obj - The object to flatten
 * @param {string} root - The base to use for selectors
 * @param {function} validator - A function to determine values to skip
 * @example
 *
 *    let myObject = {
 *      a: 1,
 *      b: {
 *        c: [1,2],
 *        d: {
 *          e: 'foo'
 *        }
 *      },
 *      f: 'fat rat'
 *    };
 *
 *    // Skip any object with { e:'foo', ...}
 *    let myValidator = (val) => !!(val.e = 'foo');
 *
 *    let result = flattenSelectors(myObject);
 *    {
 *      'a'     : 1,
 *      'b.c'   : [1,2],
 *      'b.d.e' : foo',
 *      'f'     : 'fat rat'
 *    }
 *
 *    let result = flattenSelectors(myObject, 'pig');
 *    {
 *      'pig.a'     : 1,
 *      'pig.b.c'   : [1,2],
 *      'pig.b.d.e' : foo',
 *      'pig.f'     : 'fat rat'
 *    }
 *
 *    let result = flattenSelectors(myObject, null, myValidator);
 *    {
 *      'a'   : 1,
 *      'b.c' : [1,2],
 *      'b.d' : {e:'foo'},
 *      'f'   : 'fat rat'
 *    }
 */
export const flattenSelectors = (obj, root = null, validator = null) => {
  let result = {};

  if (!validator) {
    validator = (val) => {
      if (Array.isArray(val)) {
        return true;
      } if (val === Object(val)) {
        return false;
      }
      return true;
    };
  }

  Object.keys(obj).forEach((key) => {
    let value = obj[key];

    let subkey = root ? [root, key].join('.') : key;

    if (validator(value)) {
      Object.assign(result, { [subkey]: value });
    } else {
      Object.assign(result, flattenSelectors(value, subkey, validator));
    }
  });

  return result;
};

/**
 * isMUIStyleDef(obj)
 * @arg {object} obj - An object to discern if it is a MUI style definition
 * @returns {boolean} - Whether or not this is a MUI style definition
 * @example
 *    // returns TRUE:  this *IS* a MUI style definition.
 *    isMUIStyleDef({
 *      fontFamily: 'sans-serif',
 *      fontSize: 3,
 *    })
 *
 *    // returns FALSE:  this *IS NOT* a MUI style definition
 *    isMUIStyleDef({
 *      'header': {
 *        fontFamily: 'sans-serif',
 *        fontSize: 3,
 *      }
 *    })
 */
export const isMUIStyleDef = (obj) => {
  let vals = Object.values(obj);

  for (let i = 0; i < vals.length; i += 1) {
    if (typeof vals[i] !== 'object') {
      return true;
    }
  }

  return false;
};

/**
 * Given a color, determine if it's light or dark.
 * @param {string} color - A color in rgb or hex.
 * @return {string} Either 'light' or 'dark'
 * @example
 *    if (colorLightness(color) === 'light') { ... }
 */
export const colorLightness = (color) => {
  // Variables for red, green, blue values
  let r;
  let g;
  let b;
  let hsp;

  // Check the format of the color, HEX or RGB?
  if (color.match(/^rgb/)) {
    // If RGB --> store the red, green, blue values in separate variables
    [, r, g, b] = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
  } else {
    // If hex --> Convert it to RGB: http://gist.github.com/983661
    color = +(`0x${color.slice(1).replace(color.length < 5 && /./g, '$&$&')}`);
    r = color >> 16;
    g = (color >> 8) & 255;
    b = color & 255;
  }

  // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
  hsp = Math.sqrt(
    0.299 * (r * r)
    + 0.587 * (g * g)
    + 0.114 * (b * b),
  );

  // Using the HSP value, determine whether the color is light or dark
  if (hsp > 127.5) {
    return 'light';
  }
  return 'dark';
};

/**
 * Given a background color, return a nice, readable grayscale text color
 * for it.
 * @param {string} color - A color in rgb or hex.
 * @return {string} A complementary hex color.
 * @example
 *    let textColor = textForBgColor(bgColor)
 */
export const textForBgColor = (color) => {
  if (colorLightness(color) === 'light') {
    return '#263238';
  }
  return '#efecea';
};

export default {
  renderFontFamilyStr,
  pxToRem,
  flattenSelectors,
  isMUIStyleDef,
  colorLightness,
  textForBgColor,
};
