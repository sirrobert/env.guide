/**
 * @jest-environment jsdom
 */
import {
  pxToRem,
  renderFontFamilyStr,
  flattenSelectors,
  isMUIStyleDef,
  colorLightness,
  textForBgColor,
} from './util';

/**
 * pxToRem()
 */
[
  { in: [10, 16], out: '0.6250rem' },
  { in: [12, 16], out: '0.7500rem' },
  { in: [15, 16], out: '0.9375rem' },
  { in: [15, 13], out: '1.1538rem' },
  { in: [15, 12], out: '1.2500rem' },
  { in: [12, 11], out: '1.0909rem' },
].forEach((data) => {
  test('Testing various combinations of small/large', () => {
    expect(pxToRem(...data.in)).toBe(data.out);
  });
});

/**
 * renderFontFamilyStr()
 */
[
  {
    description: 'Single non-keyword with no special chars',
    in: ['Montserrat'],
    out: 'Montserrat',
  },

  {
    description: 'Single non-keyword with special chars',
    in: ['haettenschweiler bold'],
    out: '"haettenschweiler bold"',
  },

  {
    description: 'Single keyword',
    in: ['sans-serif'],
    out: 'sans-serif',
  },

  {
    description: 'multiple keywords',
    in: ['sans-serif', 'fantasy'],
    out: 'sans-serif, fantasy',
  },

  {
    description: 'flat arg list with every kind of variation',
    in: [
      'Roboto',
      'San Francisco',
      '-apple-system',
      'BlinkMacSystemFont',
      '.SFNSText-Regular',
      'Helvetica Neue',
      'Helvetica',
      'sans-serif',
    ],
    out: 'Roboto, "San Francisco", -apple-system, BlinkMacSystemFont, ".SFNSText-Regular", "Helvetica Neue", Helvetica, sans-serif',
  },

  {
    description: 'Mixed list with every kind of variation',
    in: [
      'Roboto',
      [
        'San Francisco',
        '-apple-system',
        'BlinkMacSystemFont',
      ],
      '.SFNSText-Regular',
      'Helvetica Neue',
      ['Helvetica'],
      'sans-serif',
    ],
    out: 'Roboto, "San Francisco", -apple-system, BlinkMacSystemFont, ".SFNSText-Regular", "Helvetica Neue", Helvetica, sans-serif',
  },

  {
    description: 'Mixed array with every kind of variation',
    in: [
      [
        'Roboto',
        [
          'San Francisco',
          '-apple-system',
          'BlinkMacSystemFont',
        ],
        '.SFNSText-Regular',
        'Helvetica Neue',
        ['Helvetica'],
        'sans-serif',
      ],
    ],
    out: 'Roboto, "San Francisco", -apple-system, BlinkMacSystemFont, ".SFNSText-Regular", "Helvetica Neue", Helvetica, sans-serif',
  },

  {
    description: 'Multiple mixed arrays with every kind of variation',
    in: [
      [
        'Roboto',
        [
          'San Francisco',
          '-apple-system',
          'BlinkMacSystemFont',
        ],
      ],
      [
        '.SFNSText-Regular',
        'Helvetica Neue',
        ['Helvetica'],
        'sans-serif',
      ],
    ],
    out: 'Roboto, "San Francisco", -apple-system, BlinkMacSystemFont, ".SFNSText-Regular", "Helvetica Neue", Helvetica, sans-serif',
  },
].forEach((data) => {
  // Just loop through each object and check if ins match outs.
  test(data.description, () => {
    expect(renderFontFamilyStr(data.in)).toBe(data.out);
  });
});

test('flattenSelectors()', () => {
  let myObject = {
    a: 1,
    b: {
      c: [1, 2],
      d: {
        e: 'foo',
      },
    },
    f: 'fat rat',
  };

  // expect(flattenSelectors(myObject)).toHaveProperty('a', 1);
  // expect(flattenSelectors(myObject)).toHaveProperty('b.c', [1,2]);
  // expect(flattenSelectors(myObject)).toHaveProperty('b.d.e', 'foo');
  // expect(flattenSelectors(myObject)).toHaveProperty('f', 'fat rat');

  expect(flattenSelectors(myObject, 'pig')).toHaveProperty(['pig.a'], 1);
  expect(flattenSelectors(myObject, 'pig')).toHaveProperty(['pig.b.c'], [1, 2]);
  expect(flattenSelectors(myObject, 'pig')).toHaveProperty(['pig.b.d.e'], 'foo');
  expect(flattenSelectors(myObject, 'pig')).toHaveProperty(['pig.f'], 'fat rat');

  let myValidator = (val) => {
    if (val.e === 'foo') {
      return true;
    } if (Array.isArray(val)) {
      return true;
    } if (val === Object(val)) {
      return false;
    }
    return true;
  };

  expect(flattenSelectors(myObject, undefined, myValidator)).toHaveProperty(['a'], 1);
  expect(flattenSelectors(myObject, undefined, myValidator)).toHaveProperty(['b.c'], [1, 2]);
  expect(flattenSelectors(myObject, undefined, myValidator)).toHaveProperty(['b.d'], { e: 'foo' });
  expect(flattenSelectors(myObject, undefined, myValidator)).toHaveProperty(['f'], 'fat rat');
});

test('isMUIStyleDef()', () => {
  expect(isMUIStyleDef({ foo: { bar: 'baz' } })).toBe(false);
  expect(isMUIStyleDef({ foo: 'baz' })).toBe(true);
});

test('colorLightness(<hex-color>)', () => {
  expect(colorLightness('#ffffff')).toBe('light');
  expect(colorLightness('#abcdef')).toBe('light');
  expect(colorLightness('#024902')).toBe('dark');
  expect(colorLightness('#555555')).toBe('dark');
  expect(colorLightness('#888888')).toBe('light');

  // This one tests the three-letter variant of hex colors.
  expect(colorLightness('#aaa')).toBe('light');
});

test('colorLightness(<rgb-color>)', () => {
  expect(colorLightness('rgb(255,255,255)')).toBe('light');
  expect(colorLightness('rgb(128,255,255)')).toBe('light');
  expect(colorLightness('rgb(128,255,0)')).toBe('light');
  expect(colorLightness('rgb(127,127,0)')).toBe('dark');
  expect(colorLightness('rgb(0,0,0)')).toBe('dark');
});

test('textForBgColor(color)', () => {
  expect(textForBgColor('#ffffff')).toBe('#263238');
  expect(textForBgColor('#000000')).toBe('#efecea');
  expect(textForBgColor('rgb(0,0,0)')).toBe('#efecea');
});
